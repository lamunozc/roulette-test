
package com.test.roulette.database.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.test.roulette.database.entities.BettingEntity;

@Repository
public interface BettingRepository extends CrudRepository<BettingEntity, String> {

    List<BettingEntity> findByRouletteId(String rouletteId);

}
