
package com.test.roulette.database.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.test.roulette.database.entities.UserEntity;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, String> {

}
