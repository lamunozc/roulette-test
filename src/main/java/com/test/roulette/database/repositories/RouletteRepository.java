
package com.test.roulette.database.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.test.roulette.database.entities.RouletteEntity;

@Repository
public interface RouletteRepository extends CrudRepository<RouletteEntity, String> {
    Optional<RouletteEntity> findById(String id);

}
