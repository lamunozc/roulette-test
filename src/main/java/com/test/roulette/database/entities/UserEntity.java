
package com.test.roulette.database.entities;

import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import lombok.NoArgsConstructor;

@RedisHash
@NoArgsConstructor
public class UserEntity {

    @Indexed
    private String id;
    private Double amount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

}
