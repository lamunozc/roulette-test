
package com.test.roulette.database.entities;

import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@RedisHash
public class BettingEntity {

    private String id;
    @Indexed
    private String rouletteId;
    private String userId;
    private Double amount;
    private boolean number;
    private String bett;

    public BettingEntity(String id, String rouletteId, String userId, Double amount, boolean number,
            String bett) {
        super();
        this.id = id;
        this.rouletteId = rouletteId;
        this.userId = userId;
        this.amount = amount;
        this.number = number;
        this.bett = bett;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRouletteId() {
        return rouletteId;
    }

    public void setRouletteId(String rouletteId) {
        this.rouletteId = rouletteId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getBett() {
        return bett;
    }

    public void setBett(String bett) {
        this.bett = bett;
    }

    public boolean isNumber() {
        return number;
    }

    public void setNumber(boolean number) {
        this.number = number;
    }

}
