
package com.test.roulette.utils;

import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;

import com.test.roulette.database.entities.UserEntity;
import com.test.roulette.enumerables.ErrorCodeMessageEnum;
import com.test.roulette.exceptions.BasicException;

public class BettingHelper {

    private final static String RED_COLOR = "R";
    private final static String BLACK_COLOR = "N";

    private BettingHelper() {

    }

    public static Integer calculateNumberWinner() {
        Integer randomNum = ThreadLocalRandom.current().nextInt(0, 36 + 1);

        return randomNum;
    }

    public static Double calculateReward(boolean numberBet, String userBet, Integer numberWinner,
            Double amount) {
        Double result = 0.0;
        String colorWinner = numberWinner % 2 == 0 ? RED_COLOR : BLACK_COLOR;
        if (numberBet && Integer.valueOf(userBet) == numberWinner) {
            result = amount * 5;
        } else if (colorWinner.equals(userBet)) {
            result = amount * 1.8;
        }

        return result;
    }

    public static boolean isNumberBetting(String value) {
        Pattern p = Pattern.compile("\\\\d+");

        return p.matcher(value).matches();
    }

    public static void validetaUser(Optional<UserEntity> userEntity, Double betAmount) {
        if (userEntity.isPresent()) {
            if (userEntity.get().getAmount() < betAmount) {
                throw new BasicException(ErrorCodeMessageEnum.INSUFFICIENT_BALANCE);
            }
        } else {
            throw new BasicException(ErrorCodeMessageEnum.USER_NOT_FOUND);
        }
    }
}
