
package com.test.roulette.utils;

import java.util.Optional;

import com.test.roulette.database.entities.RouletteEntity;
import com.test.roulette.enumerables.ErrorCodeMessageEnum;
import com.test.roulette.enumerables.RouletteStatusEnum;
import com.test.roulette.exceptions.BasicException;

public class RouletteHelper {

    private RouletteHelper() {

    }

    public static void validateState(RouletteStatusEnum state,
            Optional<RouletteEntity> rouletteEntity) {
        if (rouletteEntity.isPresent()) {
            if (!state.name().equals(rouletteEntity.get().getState())) {
                throw new BasicException(ErrorCodeMessageEnum.ROULETTE_INVALID_STATE);
            }
        } else {
            throw new BasicException(ErrorCodeMessageEnum.ROULETTE_NOT_FOUND);
        }
    }
}
