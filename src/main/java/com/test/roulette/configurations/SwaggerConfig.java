package com.test.roulette.configurations;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.async.DeferredResult;

import com.fasterxml.classmate.TypeResolver;
import com.google.common.collect.Lists;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.AlternateTypeRules;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * It reads the 'api.swagger.tittle' property and uses its value for the class attribute
     * {@code apiTittle}.
     */
    @Value("${api.swagger.tittle}")
    private String apiTittle;

    /**
     * It reads the 'api.swagger.description' property and uses its value for the class attribute
     * {@code apiDescription}.
     */
    @Value("${api.swagger.description}")
    private String apiDescription;

    /**
     * It reads the 'api.swagger.version' property and uses its value for the class attribute
     * {@code apiVersion}.
     */
    @Value("${api.swagger.version}")
    private String apiVersion;

    /**
     * It reads the 'api.swagger.contact.name' property and uses its value for the class attribute
     * {@code contactName}.
     */
    @Value("${api.swagger.contact.name}")
    private String contactName;

    /**
     * It reads the 'api.swagger.contact.url' property and uses its value for the class attribute
     * {@code contactUrl}.
     */
    @Value("${api.swagger.contact.url}")
    private String contactUrl;

    /**
     * It reads the 'api.swagger.contact.email' property and uses its value for the class attribute
     * {@code contactEmail}.
     */
    @Value("${api.swagger.contact.email}")
    private String contactEmail;

    /**
     * It reads the 'api.swagger.service.rootpath' property and uses its value for the class
     * attribute {@code serviceRootpath}.
     */
    @Value("${api.swagger.service.rootpath}")
    private String serviceRootpath;

    /**
     * It reads the 'api.swagger.base-package' property and uses its value for the class attribute
     * {@code basePackage}.
     */
    @Value("${api.swagger.base-package}")
    private String basePackage;

    /**
     * It reads the 'api.swagger.service.name' property and uses its value for the class attribute
     * {@code serviceName}.
     */
    @Value("${api.swagger.service.name}")
    private String serviceName;

    @Autowired
    private TypeResolver typeResolver;

    @Bean
    @RefreshScope
    public Docket sltApi() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage(basePackage)).paths(PathSelectors.any())
                .build().pathMapping("/").apiInfo(apiInfo())
                .directModelSubstitute(LocalDate.class, java.sql.Date.class)
                .directModelSubstitute(LocalDateTime.class, java.util.Date.class)
                .genericModelSubstitutes(ResponseEntity.class)
                .alternateTypeRules(
                        AlternateTypeRules.newRule(
                                typeResolver.resolve(DeferredResult.class,
                                        typeResolver.resolve(ResponseEntity.class,
                                                WildcardType.class)),
                                typeResolver.resolve(WildcardType.class)))
                .useDefaultResponseMessages(false).tags(new Tag(serviceRootpath, serviceName))
                .globalResponseMessage(RequestMethod.GET,
                        Lists.newArrayList(
                                new ResponseMessageBuilder().code(500).message("500 message")
                                        .responseModel(new ModelRef("Error")).build()))
                .enableUrlTemplating(false);
    }

    /**
     * Indicates the api information.
     *
     * @return Api information.
     */
    private ApiInfo apiInfo() {
        return new ApiInfo(apiTittle, apiDescription, apiVersion, "",
                new Contact(contactName, contactUrl, contactEmail), "", "", Collections.emptyList());
    }

    public TypeResolver getTypeResolver() {
        return typeResolver;
    }

    public void setTypeResolver(TypeResolver typeResolver) {
        this.typeResolver = typeResolver;
    }

    public String getServiceRootpath() {
        return serviceRootpath;
    }

    public void setServiceRootpath(String serviceRootpath) {
        this.serviceRootpath = serviceRootpath;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
