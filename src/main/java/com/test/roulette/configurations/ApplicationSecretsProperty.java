
package com.test.roulette.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * This class should by create like secrets in AWS
 * 
 * @author leonardo.munoz
 *
 */
@RefreshScope
@Configuration
public class ApplicationSecretsProperty {

    @Value("${test.redis.username}")
    private String redisUsername;

    @Value("${test.redis.password}")
    private String redisPassword;

    @Value("${test.redis.url}")
    private String redisUrl;

    public String getRedisUsername() {
        return redisUsername;
    }

    public void setRedisUsername(String redisUsername) {
        this.redisUsername = redisUsername;
    }

    public String getRedisPassword() {
        return redisPassword;
    }

    public void setRedisPassword(String redisPassword) {
        this.redisPassword = redisPassword;
    }

    public String getRedisUrl() {
        return redisUrl;
    }

    public void setRedisUrl(String redisUrl) {
        this.redisUrl = redisUrl;
    }

}
