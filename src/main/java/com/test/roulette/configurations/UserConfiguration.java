
package com.test.roulette.configurations;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.test.roulette.database.entities.UserEntity;

@Component
@ConfigurationProperties(
        prefix = "list")
public class UserConfiguration {

    private List<UserEntity> users;

    public List<UserEntity> getUsers() {
        return users;
    }

    public void setUsers(List<UserEntity> users) {
        this.users = users;
    }

}
