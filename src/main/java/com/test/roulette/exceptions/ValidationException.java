package com.test.roulette.exceptions;

public class ValidationException extends RuntimeException {
	private static final long serialVersionUID = -2582859164292341503L;

	private final String message;

	public ValidationException(String message) {
		super();
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
