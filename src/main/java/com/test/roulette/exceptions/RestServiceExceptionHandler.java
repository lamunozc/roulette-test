
package com.test.roulette.exceptions;

import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.roulette.dto.ErrorResponse;
import com.test.roulette.enumerables.ErrorCodeMessageEnum;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice(
        annotations = RestController.class)
public class RestServiceExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestServiceExceptionHandler.class);

    @ExceptionHandler(BasicException.class)
    protected ResponseEntity<ErrorResponse> basicError(BasicException error) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setCode(String.valueOf(error.getCode()));
        errorResponse.setMessage(error.getMessage());
        LOGGER.error(getMessage(HttpStatus.BAD_REQUEST.value(), String.valueOf(error.getCode()),
                error.getMessage()));
        return ResponseEntity.ok(errorResponse);
    }

    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> processValidationError(
            final HttpServletRequest httpServletRequest,
            final HttpServletResponse httpServletResponse,
            final MethodArgumentNotValidException ex) {
        String errorMessage = ex.getBindingResult().getAllErrors().stream()
                .map(error -> error instanceof FieldError
                        ? ((FieldError) error).getField() + " " + error.getDefaultMessage()
                        : error.getDefaultMessage())
                .collect(Collectors.joining(", "));
        String code = String.valueOf(ErrorCodeMessageEnum.VALIDATION_MESSAGE.getCode());

        LOGGER.error(getMessage(HttpStatus.BAD_REQUEST.value(), code, errorMessage));

        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setCode(code);
        errorResponse.setMessage(errorMessage);
        return ResponseEntity.badRequest().body(errorResponse);
    }

    private String getMessage(int httpStatus, String error, String message) {
        return String.format("http status:[%d] - Code: [%s] Message: %s", httpStatus, error,
                message);
    }
}
