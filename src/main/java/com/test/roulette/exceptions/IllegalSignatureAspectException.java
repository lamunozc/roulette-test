package com.test.roulette.exceptions;

public class IllegalSignatureAspectException extends IllegalArgumentException {
	private static final long serialVersionUID = -3614289488980893361L;
	private static final String DEVELOPMENT_ERROR = "Development error";
	public static final String VALIDATION_PARAMETERS_ERROR = "The %s is mandatory to %s on parameters. Checkout %s method.";
	public static final String VALIDATION_RETURN_ERROR = "The %s is mandatory to %s on return value. Checkout %s method.";
	
	private final String errorMessage;
	
	private final String kindValue;
	
	private final String aspectClassName;
	
	private final String methodName;

	public IllegalSignatureAspectException(String errorMessage, String kindValue, String aspectClassName, String methodName) {
		this.errorMessage = errorMessage;
		this.kindValue = kindValue;
		this.aspectClassName = aspectClassName;
		this.methodName = methodName;
	}
	
	@Override
	public String getMessage() {
		return DEVELOPMENT_ERROR;
	}
	
	@Override
	public synchronized Throwable getCause() {
		return new Throwable(String.format(errorMessage, kindValue, aspectClassName, methodName));
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public String getKindValue() {
		return kindValue;
	}

	public String getAspectClassName() {
		return aspectClassName;
	}

	public String getMethodName() {
		return methodName;
	}
}
