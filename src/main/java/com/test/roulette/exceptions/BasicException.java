
package com.test.roulette.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.test.roulette.enumerables.ErrorCodeMessageEnum;

@ResponseStatus(
        value = HttpStatus.BAD_REQUEST)
public class BasicException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private final int code;
    private final String message;

    public BasicException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.message = message;
    }

    public BasicException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public BasicException(ErrorCodeMessageEnum errorCodeEnum) {
        this.code = errorCodeEnum.getCode();
        this.message = errorCodeEnum.getMessage();
    }

    @Override
    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

}
