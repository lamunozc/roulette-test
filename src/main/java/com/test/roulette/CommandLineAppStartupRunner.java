
package com.test.roulette;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.test.roulette.configurations.UserConfiguration;
import com.test.roulette.database.repositories.UserRepository;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {
    private static final Logger LOG = LoggerFactory.getLogger(CommandLineAppStartupRunner.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserConfiguration userConfiguration;

    @Override
    public void run(String... args) throws Exception {
        LOG.info("Loading users started...");
        userConfiguration.getUsers().forEach(userEntity -> userRepository.save(userEntity));
        LOG.info("Loading users OK...");
    }
}
