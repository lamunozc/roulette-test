
package com.test.roulette.enumerables;

public enum RouletteStatusEnum {
    CREATED, OPEN, CLOSE;
}
