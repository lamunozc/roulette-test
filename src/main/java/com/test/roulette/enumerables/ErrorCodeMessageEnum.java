
package com.test.roulette.enumerables;

public enum ErrorCodeMessageEnum {
    GENERAL_ERROR(901, "Consulte con el administrador del sistema"),
    VALIDATION_MESSAGE(902, "Error de validacion"),
    USER_NOT_FOUND(903, "El usuario no existe"),
    INSUFFICIENT_BALANCE(904, "El usuario no cuenta con saldo suficiente"),
    ROULETTE_INVALID_STATE(905, "La ruleta tiene un estado invalido para esta operacion"),
    ROULETTE_NOT_FOUND(906, "La ruleta no existe");

    private final int code;
    private final String message;

    private ErrorCodeMessageEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getCodeToString() {
        return String.valueOf(code);
    }
}
