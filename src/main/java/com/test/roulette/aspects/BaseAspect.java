
package com.test.roulette.aspects;

import java.lang.annotation.Annotation;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.web.bind.annotation.RequestBody;

import com.test.roulette.exceptions.IllegalSignatureAspectException;

public class BaseAspect {
    public <T extends Annotation> T getAnnotation(JoinPoint joinPoint, Class<T> annotation) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        String methodName = signature.getMethod().getName();
        Class<?>[] parameterTypes = signature.getMethod().getParameterTypes();
        try {

            return joinPoint.getTarget().getClass().getMethod(methodName, parameterTypes)
                    .getAnnotation(annotation);
        } catch (NoSuchMethodException | SecurityException e) {

            return null;
        }
    }

    /**
     * Get anotations with joinPoint entry
     * 
     * @param joinPoint
     * @return
     */
    protected Annotation[][] getAnnotations(JoinPoint joinPoint) {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();

        return methodSignature.getMethod().getParameterAnnotations();
    }

    /**
     * Get body from requestBody
     * 
     * @param joinPoint
     * @param paramAnnotations
     * @return
     */
    protected Object getBodyFromParameters(JoinPoint joinPoint,
            final Annotation[][] paramAnnotations) {
        for (int i = 0; i < paramAnnotations.length; i++) {
            for (Annotation a : paramAnnotations[i]) {
                if (a instanceof RequestBody) {

                    return joinPoint.getArgs()[i];
                }
            }
        }

        return null;
    }

    /**
     * Validate that request is not empty httpServletRequest
     * 
     * @param parameters
     * @return
     */
    protected HttpServletRequest getHttpServletRequestFromParameters(Object[] parameters) {
        for (Object object : parameters) {
            if (object instanceof HttpServletRequest) {

                return (HttpServletRequest) object;
            }
        }
        throw new IllegalSignatureAspectException(
                IllegalSignatureAspectException.VALIDATION_PARAMETERS_ERROR,
                HttpServletRequest.class.getSimpleName(), this.getClass().getSimpleName(),
                "logginControllerRequest");
    }
}
