
package com.test.roulette.aspects;

import org.aspectj.lang.annotation.Pointcut;

public interface ControllerPointcut {
    /**
     * Class that apply pointcut
     */
    @Pointcut("execution(* com.test.roulette.controllers.interfaces..*.*(..))")
    public default void controller() {
        // Empty method
    }
}
