
package com.test.roulette.aspects;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.util.Strings;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.google.gson.Gson;
import com.test.roulette.constants.Header;

@Aspect
@Component
public class LoggerAspect extends BaseAspect implements ControllerPointcut {
    /**
     * Print request and response using joinPoint
     * 
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    @Around("controller()")
    public Object logginControllerRequest(ProceedingJoinPoint joinPoint) throws Throwable {
        Logger logger = LoggerFactory.getLogger(joinPoint.getTarget().getClass());

        HttpServletRequest request = getHttpServletRequestFromParameters(joinPoint.getArgs());
        String serviceName = request.getRequestURI();
        String methodType = request.getMethod();
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        Enumeration<String> headerNames = request.getHeaderNames();
        Enumeration<String> parameterNames = request.getParameterNames();
        String userId = request.getHeader(Header.USER_ID);
        String headers = getHeaders(request, headerNames);
        String params = getParams(request, parameterNames);
        final Annotation[][] paramAnnotations = method.getParameterAnnotations();
        // Find body request on parameters
        Object requestBody = getBodyFromParameters(joinPoint, paramAnnotations);
        String requestJson = "Request" + new Gson().toJson(new Gson().toJson(requestBody));
        String headerJson = "Header" + new Gson().toJson(new Gson().toJson(headers));
        String paramsJson = "Params" + new Gson().toJson(new Gson().toJson(params));
        // Logging request
        if (!headers.isEmpty()) {
            logger.debug("[" + methodType + "] [" + serviceName + "]",
                    deleteBracesFromJson(headerJson));
        }
        if (!params.isEmpty()) {
            logger.debug("[" + methodType + "] [" + serviceName + "]",
                    deleteBracesFromJson(paramsJson));
        }
        logger.info(
                "[" + methodType + "] [" + serviceName + "] " + deleteBracesFromJson(requestJson));
        if (userId != null) {
            logger.info("[ USER_ID ] " + userId);
        }
        Object response = joinPoint.proceed(joinPoint.getArgs());
        // Logging response
        String responseJson = responseToJson(response);
        logger.info(
                "[" + methodType + "] [" + serviceName + "] " + deleteBracesFromJson(responseJson));

        return response;
    }

    private String responseToJson(Object response) {
        if (response instanceof ResponseEntity) {
            Object bodyResponse = ((ResponseEntity<?>) response).getBody();

            return "Response" + new Gson().toJson(new Gson().toJson(bodyResponse));
        } else {

            return "Response" + new Gson().toJson(new Gson().toJson(response));
        }
    }

    private Object getBodyFromParameters(ProceedingJoinPoint joinPoint,
            final Annotation[][] paramAnnotations) {
        for (int i = 0; i < paramAnnotations.length; i++) {
            for (Annotation a : paramAnnotations[i]) {
                if (a instanceof RequestBody) {

                    return joinPoint.getArgs()[i];
                }
            }
        }
        return null;
    }

    private String deleteBracesFromJson(String json) {
        return json.substring(0, json.length() - 1).replace("\\", Strings.EMPTY);
    }

    @SuppressWarnings("unchecked")
    private String getHeaders(HttpServletRequest request, Enumeration<String> headerNames) {
        JSONObject buildHeaders = new JSONObject();

        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            buildHeaders.put(headerName, request.getHeader(headerName));
        }
        if (buildHeaders.isEmpty()) {
            return Strings.EMPTY;
        }

        return buildHeaders.toString();
    }

    @SuppressWarnings("unchecked")
    private String getParams(HttpServletRequest request, Enumeration<String> paramNames) {
        JSONObject buildHeaders = new JSONObject();

        while (paramNames.hasMoreElements()) {
            String paramName = paramNames.nextElement();
            buildHeaders.put(paramName, request.getParameter(paramName));
        }
        if (buildHeaders.isEmpty()) {
            return Strings.EMPTY;
        }

        return buildHeaders.toString();
    }
}
