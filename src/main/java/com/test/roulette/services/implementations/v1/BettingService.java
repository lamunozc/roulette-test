
package com.test.roulette.services.implementations.v1;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.test.roulette.database.entities.BettingEntity;
import com.test.roulette.database.entities.RouletteEntity;
import com.test.roulette.database.entities.UserEntity;
import com.test.roulette.database.repositories.BettingRepository;
import com.test.roulette.database.repositories.RouletteRepository;
import com.test.roulette.database.repositories.UserRepository;
import com.test.roulette.dto.BaseResponse;
import com.test.roulette.dto.BettingRequest;
import com.test.roulette.dto.BettingResponse;
import com.test.roulette.dto.CloseBettingResponse;
import com.test.roulette.dto.RouletteRequest;
import com.test.roulette.enumerables.RouletteStatusEnum;
import com.test.roulette.services.interfaces.v1.IBettingService;
import com.test.roulette.utils.BettingHelper;
import com.test.roulette.utils.RouletteHelper;

@Service
public class BettingService implements IBettingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BettingService.class);

    private RouletteRepository rouletteRepository;
    private BettingRepository bettingRepository;
    private UserRepository userRepository;

    public BettingService(RouletteRepository rouletteRepository,
            BettingRepository bettingRepository, UserRepository userRepository) {
        this.rouletteRepository = rouletteRepository;
        this.bettingRepository = bettingRepository;
        this.userRepository = userRepository;
    }

    @Override
    public BaseResponse betting(String userId, String transactionId, BettingRequest request) {
        Optional<RouletteEntity> findRouletteById =
                rouletteRepository.findById(request.getIdRoulette());
        RouletteHelper.validateState(RouletteStatusEnum.OPEN, findRouletteById);
        Optional<UserEntity> findUserById = userRepository.findById(userId);
        BettingHelper.validetaUser(findUserById, request.getAmount());
        BettingEntity bettingEntity = new BettingEntity(UUID.randomUUID().toString(),
                request.getIdRoulette(), userId, request.getAmount(),
                BettingHelper.isNumberBetting(request.getBet()), request.getBet());
        bettingRepository.save(bettingEntity);
        BaseResponse response = new BaseResponse();
        response.setStatus("OK");
        response.setTransactionId(transactionId);
        response.setTransactionDate(new Date());
        return response;
    }

    @Override
    public CloseBettingResponse closeBetting(String transactionId, RouletteRequest request) {
        CloseBettingResponse response = new CloseBettingResponse();
        response.setListBetting(new ArrayList<>());
        Optional<RouletteEntity> findById = rouletteRepository.findById(request.getId());
        RouletteHelper.validateState(RouletteStatusEnum.OPEN, findById);
        Integer calculateWinner = BettingHelper.calculateNumberWinner();
        LOGGER.info("The number winner is: " + calculateWinner);
        response.setResult(calculateWinner.toString());
        List<BettingEntity> findByRouletteId = bettingRepository.findByRouletteId(request.getId());
        findByRouletteId.forEach(betting -> response.getListBetting()
                .add(new BettingResponse(betting.getUserId(),
                        BettingHelper.calculateReward(betting.isNumber(), betting.getBett(),
                                calculateWinner, betting.getAmount()))));
        RouletteEntity rouletteEntity = findById.get();
        rouletteEntity.setState(RouletteStatusEnum.CLOSE.name());
        rouletteRepository.save(rouletteEntity);
        return response;
    }

}
