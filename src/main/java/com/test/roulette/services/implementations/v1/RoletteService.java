
package com.test.roulette.services.implementations.v1;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.test.roulette.database.entities.RouletteEntity;
import com.test.roulette.database.repositories.RouletteRepository;
import com.test.roulette.dto.BaseResponse;
import com.test.roulette.dto.ListRouletteResponse;
import com.test.roulette.dto.RouletteRequest;
import com.test.roulette.dto.RouletteResponse;
import com.test.roulette.enumerables.RouletteStatusEnum;
import com.test.roulette.services.interfaces.v1.IRoletteService;
import com.test.roulette.utils.RouletteHelper;

@Service
public class RoletteService implements IRoletteService {

    private RouletteRepository rouletteRepository;

    public RoletteService(RouletteRepository rouletteRepository) {
        this.rouletteRepository = rouletteRepository;
    }

    @Override
    public RouletteResponse createRoulette(String transactionId) {

        RouletteEntity rouletteEntity =
                new RouletteEntity(UUID.randomUUID().toString(), RouletteStatusEnum.CREATED.name());

        return new RouletteResponse(rouletteRepository.save(rouletteEntity));
    }

    @Override
    public ListRouletteResponse listRoulette(String transactionId) {

        ListRouletteResponse listRouletteResponse = new ListRouletteResponse();
        List<RouletteResponse> listRoulette = new ArrayList<>();
        rouletteRepository.findAll().forEach(roulette -> listRoulette
                .add(new RouletteResponse(roulette.getId(), roulette.getState())));
        listRouletteResponse.setListRoulette(listRoulette);
        listRouletteResponse.setStatus("OK");
        listRouletteResponse.setTransactionId(transactionId);
        listRouletteResponse.setTransactionDate(new Date());

        return listRouletteResponse;
    }

    @Override
    public BaseResponse openRoulette(String transactionId, RouletteRequest request) {

        Optional<RouletteEntity> findById = rouletteRepository.findById(request.getId());
        RouletteHelper.validateState(RouletteStatusEnum.CREATED, findById);
        findById.get().setState(RouletteStatusEnum.OPEN.name());
        rouletteRepository.save(findById.get());
        BaseResponse response = new BaseResponse();
        response.setStatus("OK");
        response.setTransactionId(transactionId);
        response.setTransactionDate(new Date());

        return response;
    }

}
