
package com.test.roulette.services.interfaces.v1;

import com.test.roulette.dto.BaseResponse;
import com.test.roulette.dto.BettingRequest;
import com.test.roulette.dto.CloseBettingResponse;
import com.test.roulette.dto.RouletteRequest;

public interface IBettingService {
    BaseResponse betting(String userId, String transactionId, BettingRequest request);

    CloseBettingResponse closeBetting(String transactionId, RouletteRequest request);

}
