
package com.test.roulette.services.interfaces.v1;

import com.test.roulette.dto.BaseResponse;
import com.test.roulette.dto.ListRouletteResponse;
import com.test.roulette.dto.RouletteRequest;
import com.test.roulette.dto.RouletteResponse;

public interface IRoletteService {
    BaseResponse openRoulette(String transactionId, RouletteRequest request);

    RouletteResponse createRoulette(String transactionId);

    ListRouletteResponse listRoulette(String transactionId);

}
