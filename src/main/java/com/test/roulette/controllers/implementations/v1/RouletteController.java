
package com.test.roulette.controllers.implementations.v1;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.roulette.constants.Header;
import com.test.roulette.controllers.interfaces.v1.IRouletteController;
import com.test.roulette.dto.BaseResponse;
import com.test.roulette.dto.BettingRequest;
import com.test.roulette.dto.CloseBettingResponse;
import com.test.roulette.dto.ListRouletteResponse;
import com.test.roulette.dto.RouletteRequest;
import com.test.roulette.dto.RouletteResponse;
import com.test.roulette.services.interfaces.v1.IBettingService;
import com.test.roulette.services.interfaces.v1.IRoletteService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/roulette/v1")
@Api(
        tags = {
            "roulette"
        })
public class RouletteController implements IRouletteController {
    @Autowired
    private IRoletteService rouletteService;

    @Autowired
    private IBettingService bettingService;

    @Override
    public ResponseEntity<RouletteResponse> createRoulette(HttpServletRequest request,
            @RequestHeader(Header.TRANSACTION_ID_KEY) @Valid @NonNull String transactionId) {
        RouletteResponse response = rouletteService.createRoulette(transactionId);

        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @Override
    public ResponseEntity<ListRouletteResponse> listRoulette(HttpServletRequest request,
            @RequestHeader(Header.TRANSACTION_ID_KEY) @Valid @NonNull String transactionId) {
        ListRouletteResponse response = rouletteService.listRoulette(transactionId);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<BaseResponse> openRoulette(HttpServletRequest request,
            @RequestHeader(Header.TRANSACTION_ID_KEY) @Valid @NonNull String transactionId,
            @ApiParam(
                    value = "RouletteRequest object that needs to be added",
                    required = true) @Valid @RequestBody RouletteRequest body) {
        BaseResponse response = rouletteService.openRoulette(transactionId, body);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<BaseResponse> betting(HttpServletRequest request,
            @RequestHeader(Header.USER_ID) @Valid @NonNull String userId,
            @RequestHeader(Header.TRANSACTION_ID_KEY) @Valid @NonNull String transactionId,
            @ApiParam(
                    value = "BettingRequest object that needs to be added",
                    required = true) @Valid @RequestBody BettingRequest body) {
        BaseResponse response = bettingService.betting(userId, transactionId, body);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<CloseBettingResponse> closeBetting(HttpServletRequest request,
            @RequestHeader(Header.TRANSACTION_ID_KEY) @Valid @NonNull String transactionId,
            @ApiParam(
                    value = "RouletteRequest object that needs to be added",
                    required = true) @Valid @RequestBody RouletteRequest body) {
        CloseBettingResponse response = bettingService.closeBetting(transactionId, body);
        return ResponseEntity.ok(response);
    }

}
