
package com.test.roulette.dto;

import java.util.List;

import io.swagger.annotations.ApiModel;

@ApiModel
public class CloseBettingResponse {

    private List<BettingResponse> listBetting;
    private String result;

    public List<BettingResponse> getListBetting() {
        return listBetting;
    }

    public void setListBetting(List<BettingResponse> listBetting) {
        this.listBetting = listBetting;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

}
