package com.test.roulette.dto;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * ErrorResponse
 */
@Validated
public class ErrorResponse {
	@JsonProperty("code")
	private String code = null;

	@JsonProperty("message")
	private String message = null;

	public ErrorResponse code(String code) {
		this.code = code;
		return this;
	}

	/**
	 * Error code
	 * 
	 * @return code
	 **/
	@ApiModelProperty(value = "Error code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ErrorResponse message(String message) {
		this.message = message;
		return this;
	}

	/**
	 * Error message
	 * 
	 * @return message
	 **/
	@ApiModelProperty(value = "Error message")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
