
package com.test.roulette.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

import org.springframework.validation.annotation.Validated;

/**
 * QRCodeEntityResponse
 */
@Validated
public class BettingRequest {

    @NotNull(
            message = "La apuesta no puede ser nula")
    @Pattern(
            regexp = "^(?:[0-9]|[12][0-9]|3[0-6])$|[N|R]",
            message = "Las opciones para apostar son: N(Negro), R(Rojo) � digitos entre 0 y 36")
    private String bet = null;

    @NotNull(
            message = "La apuesta no puede ser nula")
    @Positive(
            message = "La apuesta debe ser mayor que cero")
    private Double amount = null;

    @NotNull(
            message = "El id de ruleta no puede ser nula")
    private String idRoulette = null;

    public String getBet() {
        return bet;
    }

    public void setBet(String bet) {
        this.bet = bet;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getIdRoulette() {
        return idRoulette;
    }

    public void setIdRoulette(String idRoulette) {
        this.idRoulette = idRoulette;
    }

}
