
package com.test.roulette.dto;

import java.util.List;

import org.springframework.validation.annotation.Validated;

@Validated
public class ListRouletteResponse extends BaseResponse {

    private List<RouletteResponse> listRoulette;

    public List<RouletteResponse> getListRoulette() {
        return listRoulette;
    }

    public void setListRoulette(List<RouletteResponse> listRoulette) {
        this.listRoulette = listRoulette;
    }

}
