
package com.test.roulette.dto;

import javax.validation.constraints.NotNull;

public class RouletteRequest {

    @NotNull(
            message = "El id de ruleta no puede ser nulo")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
