
package com.test.roulette.dto;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class BaseResponse {

    private String status;
    private String transactionId;
    private Date transactionDate;

    @ApiModelProperty(
            dataType = "String",
            value = "Status for this response",
            example = "OK")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @ApiModelProperty(
            dataType = "String",
            value = "TransactionId for this request",
            example = "123466789")
    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @ApiModelProperty(
            value = "TransactionDate made for this response",
            dataType = "java.lang.String")
    public Date getTransactionDate() {
        return (Date) transactionDate.clone();
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = new Date(transactionDate.getTime());
    }

}
