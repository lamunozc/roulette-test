
package com.test.roulette.dto;

public class BettingResponse {

    private String userId;
    private Double winnerAmount;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Double getWinnerAmount() {
        return winnerAmount;
    }

    public void setWinnerAmount(Double winnerAmount) {
        this.winnerAmount = winnerAmount;
    }

    public BettingResponse(String userId, Double winnerAmount) {
        super();
        this.userId = userId;
        this.winnerAmount = winnerAmount;
    }

}
