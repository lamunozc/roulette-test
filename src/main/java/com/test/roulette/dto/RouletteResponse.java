
package com.test.roulette.dto;

import org.springframework.validation.annotation.Validated;

import com.test.roulette.database.entities.RouletteEntity;

@Validated
public class RouletteResponse {

    public RouletteResponse(String id, String state) {
        super();
        this.id = id;
        this.state = state;
    }

    public RouletteResponse(RouletteEntity rouletteEntity) {
        this.id = rouletteEntity.getId();
        this.state = rouletteEntity.getState();
    }

    private String id;
    private String state;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

}
