
package com.test.roulette.constants;

public class Operation {
    public static final String OPERATION_CREATE_ROULETTE = "create";
    public static final String OPERATION_OPEN_ROULETTE = "open";
    public static final String OPERATION_CLOSE_ROULETTE = "close";
    public static final String OPERATION_BETTING = "betting";
    public static final String OPERATION_LIST_ROULETTE = "list";

    /**
     * Private constructor to avoid instances of this class.
     */
    private Operation() {
        throw new UnsupportedOperationException();
    }
}
