
package com.test.roulette.constants;

public class Header {
    public static final String TRANSACTION_ID_KEY = "transactionId";
    public static final String USER_ID = "userId";

    private Header() {
    }
}
