FROM openjdk:8-jre-alpine

RUN apk upgrade --no-cache && apk upgrade --no-cache openjdk8-jre && apk add --no-cache tzdata

WORKDIR /opt/masi

ADD build/libs/test-project-*.jar test-project.jar

ENTRYPOINT ["sh", "runService.sh"]
