### Built With

The application was developed using the following tools:
- [SpringBoot](https://spring.io/projects/spring-boot)
- [Gradle](https://gradle.org/)
- [Redis](https://redis.io/)
- [Docker](https://www.docker.com/)
- [AWS Secrets](https://aws.amazon.com/es/secrets-manager/)


## Getting Started


### Prerequisites

In order to run the application, you need:
- gradle 4.4
- java
- redis

### Installation

1. Clone the repo
   ```
   git clone https://bitbucket.org/lamunozc/roulette-test/src/master/
   ```
2. Compile sources
   ```gradle clean build
   ```
3. Run redis-server Windows
   ``` cd deployment\redis-2.4.5-win32-win64\64bit\
   redis-server.exe
   ```
4. Run jar
   ```java -jar build/libs/test-project-0.0.1-BETA.jar
   ```
5. Open the browser and go to the url, to see the api in swagger http://localhost:8443/api-roulette/swagger-ui.html#
   ```http://localhost:8443/api-roulette/swagger-ui.html#
   ```

## Usage

There are 5 apis for this project:
 * /roulette/v1/create
 * /roulette/v1/open
 * /roulette/v1/list
 * /roulette/v1/betting
 * /roulette/v1/closeBetting
 
The users enabled to bet are the ids 111 and 999, if you want to add more you can do it by modifying the list in src\main\resources\bootstrap.yml in ${list.users...}
 
## Contact

Leonardo Muñoz - [Linkedin](https://www.linkedin.com/in/lamunozc/) leonardo.muoz062@gmail.com



